#ifndef GHETTO_CUDA_UTIL_NVCC_RESTRICTED_CUH
#define GHETTO_CUDA_UTIL_NVCC_RESTRICTED_CUH
#include <cuda_runtime.h>
#include <cstddef>

// Any source file that includes this header file should only be compiled using
// CUDA compiler
namespace ghetto {
template <class T, int dim, enum cudaTextureReadMode readMode>
void bindCudaTexture(const texture<T, dim, readMode> &tex, const T *device_ptr,
                     std::size_t element_count) {
  checkCudaErrors(
      cudaBindTexture(0, tex, device_ptr, element_count * sizeof(T)));
}

template <class T, int dim, enum cudaTextureReadMode readMode>
void unbindCudaTexture(const texture<T, dim, readMode> &tex) {
  checkCudaErrors(cudaUnbindTexture(tex));
}
}  // namespace ghetto
#endif
