/**
 * This header file is a wrapper for cuda_runtime.h,
 * which is required by host compilers to recognize
 * classes and types defined in CUDA.
 */
#ifndef GHETTO_CUDA_RUNTIME_HPP
#define GHETTO_CUDA_RUNTIME_HPP

#include <cuda_runtime.h>

#ifdef _MSC_VER
using uint = unsigned int;
#endif

#endif

