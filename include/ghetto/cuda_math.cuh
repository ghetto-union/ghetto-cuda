/**
 * This header file can be used by both host and NVCC compilers.
 * Afterall, it is necessary to include cuda_runtime.h for CUDA vector (float3)
 * support in host code.
 */
#ifndef GHETTO_CUDA_MATH_CUH
#define GHETTO_CUDA_MATH_CUH
#ifndef __NVCC__
#include <cmath>
#endif
#include <vector_functions.h>

// namespace is not used for convenience
constexpr __host__ __device__ float3 make_float3(float4 a) {
  return {a.x, a.y, a.z};
}

constexpr __host__ __device__ float4 make_float4(float3 a) {
  return {a.x, a.y, a.z, 0.0f};
}

constexpr __host__ __device__ int3 operator+(int3 a, int3 b) {
  return {a.x + b.x, a.y + b.y, a.z + b.z};
}

constexpr __host__ __device__ uint3 operator+(uint3 a, uint3 b) {
  return {a.x + b.x, a.y + b.y, a.z + b.z};
}

constexpr __host__ __device__ float3 operator+(float3 a, float3 b) {
  return {a.x + b.x, a.y + b.y, a.z + b.z};
}

inline __host__ __device__ void operator+=(float3& a, float3 b) {
  a.x += b.x;
  a.y += b.y;
  a.z += b.z;
}

inline __host__ __device__ void operator+=(float4& a, float4 b) {
  a.x += b.x;
  a.y += b.y;
  a.z += b.z;
}

inline __host__ __device__ void operator+=(float4& a, float3 b) {
  a.x += b.x;
  a.y += b.y;
  a.z += b.z;
}

inline __host__ __device__ void operator/=(float2& a, float b) {
  a.x /= b;
  a.y /= b;
}

inline __host__ __device__ void operator/=(float4& a, float b) {
  a.x /= b;
  a.y /= b;
  a.z /= b;
}

constexpr __host__ __device__ int3 operator-(int3 a, int3 b) {
  return {a.x - b.x, a.y - b.y, a.z - b.z};
}

constexpr __host__ __device__ float3 operator-(float3 a, float3 b) {
  return {a.x - b.x, a.y - b.y, a.z - b.z};
}

inline __host__ __device__ void operator-=(float3& a, float3 b) {
  a.x -= b.x;
  a.y -= b.y;
  a.z -= b.z;
}

constexpr __host__ __device__ float3 operator*(float b, int3 a) {
  return {b * a.x, b * a.y, b * a.z};
}

constexpr __host__ __device__ float3 operator*(float b, float3 a) {
  return {b * a.x, b * a.y, b * a.z};
}

constexpr __host__ __device__ float2 operator*(float b, float2 a) {
  return {b * a.x, b * a.y};
}

constexpr __host__ __device__ float2 operator/(float2 a, float b) {
  return {a.x / b, a.y / b};
}

constexpr __host__ __device__ float3 operator/(float3 a, float b) {
  return {a.x / b, a.y / b, a.z / b};
}

constexpr __host__ __device__ float dot(float3 a, float3 b) {
  return a.x * b.x + a.y * b.y + a.z * b.z;
}

constexpr __host__ __device__ float3 cross(float3 a, float3 b) {
  return {a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x};
}

constexpr __host__ __device__ float sign(float a) {
  return a < 0.0f ? -1.0f : (a > 0.0f ? 1.0f : 0.0f);
}

inline __host__ __device__ float3 normalize(float3 v) {
#ifdef __NVCC__
  return rsqrtf(dot(v, v)) * v;
#else
  return (v / std::sqrt(dot(v, v)));
#endif
}

constexpr __host__ __device__ float normSquared(float3 a) {
  return a.x * a.x + a.y * a.y + a.z * a.z;
}

#ifdef _MSC_VER
inline
#else
constexpr
#endif
    __host__ __device__ float
    norm(float3 a) {
#ifdef __NVCC__
  return sqrt(normSquared(a));
#else
  return std::sqrt(normSquared(a));
#endif
}
constexpr __host__ __device__ float euclideanDistanceSquared(float3 a,
                                                             float3 b) {
  return normSquared(a - b);
}

#ifdef _MSC_VER
inline
#else
constexpr
#endif
    __host__ __device__ float
    euclideanDistance(float3 a, float3 b) {
  return norm(a - b);
}

constexpr __device__ __host__ float lerp(float a, float b, float t) {
  return a + t * (b - a);
}

constexpr __device__ __host__ float3 lerp(float3 a, float3 b, float t) {
  return a + t * (b - a);
}

constexpr __host__ __device__ float4 operator+(float4 a, float4 b) {
  return {a.x + b.x, a.y + b.y, a.z + b.z, 0.0f};
}

constexpr __host__ __device__ float4 operator-(float4 a, float4 b) {
  return {a.x - b.x, a.y - b.y, a.z - b.z, 0.0f};
}

constexpr __host__ __device__ float4 operator*(float b, float4 a) {
  return {b * a.x, b * a.y, b * a.z, 0.0f};
}

constexpr __host__ __device__ float4 operator/(float4 a, float b) {
  return {a.x / b, a.y / b, a.z / b, 0.0f};
}
#endif
