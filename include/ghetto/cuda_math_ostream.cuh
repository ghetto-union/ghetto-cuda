#ifndef GHETTO_CUDA_MATH_OSTREAM_CUH
#define GHETTO_CUDA_MATH_OSTREAM_CUH
#include <iostream>
std::ostream& operator<<(std::ostream& os, float4 a) {
  os << "(" << a.x << ", " << a.y << ", " << a.z << ", " << a.w << ")";
  return os;
}

std::ostream& operator<<(std::ostream& os, float3 a) {
  os << "(" << a.x << ", " << a.y << ", " << a.z << ")";
  return os;
}
#endif
