#ifndef GHETTO_CUDA_UTIL_CUH
#define GHETTO_CUDA_UTIL_CUH
#include <cuda_runtime.h>
#include <cstddef>
namespace ghetto {
static const char *getCudaErrorString(cudaError_t error);

void __checkCudaErrors(cudaError_t result, char const *const func,
                       const char *const file, int const line);
#define checkCudaErrors(val) __checkCudaErrors((val), #val, __FILE__, __LINE__)

void __getLastCudaError(const char *errorMessage, const char *file,
                        const int line);
#define getLastCudaError(msg) __getLastCudaError(msg, __FILE__, __LINE__)

template <class DestinationT, class SourceT>
void copyCudaArray(DestinationT *dst_ptr, SourceT const *src_ptr,
                   std::size_t element_count, cudaMemcpyKind kind) {
  checkCudaErrors(cudaMemcpy(reinterpret_cast<void *>(dst_ptr),
                             reinterpret_cast<void const *>(src_ptr),
                             element_count * sizeof(DestinationT), kind));
}

template <class T>
void allocateDeviceArray(T **device_ptr, std::size_t element_count) {
  checkCudaErrors(cudaMalloc(reinterpret_cast<void **>(device_ptr),
                             element_count * sizeof(T)));
}

template <class T>
void freeDeviceArray(T **device_ptr) {
  if (*device_ptr == nullptr) return;
  checkCudaErrors(cudaFree(reinterpret_cast<void *>(*device_ptr)));
  *device_ptr = nullptr;
}

template <class T>
void reallocateDeviceArray(T **device_ptr, std::size_t element_count) {
  freeDeviceArray(device_ptr);
  allocateDeviceArray(device_ptr, element_count);
}

template <class T>
void reallocateDeviceArray(T **device_ptr, std::size_t element_count,
                           std::size_t old_element_count) {
  T *old_device_ptr = *device_ptr;
  allocateDeviceArray(device_ptr, element_count);
  if (old_element_count > 0) {
    copyCudaArray(*device_ptr, old_device_ptr, old_element_count,
                  cudaMemcpyDeviceToDevice);
  }
  freeDeviceArray(&old_device_ptr);
}

template <class DeviceT, class HostT>
void appendToDeviceArray(DeviceT **device_ptr, HostT const *host_ptr,
                         std::size_t old_capacity, std::size_t new_capacity,
                         std::size_t old_element_count,
                         std::size_t addition_count) {
  if (new_capacity > old_capacity) {
    reallocateDeviceArray(device_ptr, new_capacity, old_element_count);
  }
  copyCudaArray(*device_ptr + old_element_count, host_ptr, addition_count,
                cudaMemcpyHostToDevice);
}

template <class T>
void setCudaArray(T *device_ptr, int v, std::size_t element_count) {
  checkCudaErrors(cudaMemset(device_ptr, v, element_count * sizeof(T)));
}

// TODO: move to a better header file (math?)
unsigned int nextPowerOfTwo(unsigned int v);
void computeGridSize(unsigned int, unsigned int, unsigned int &,
                     unsigned int &);
}  // namespace ghetto
#endif
